package ru.t1.kruglikov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.model.ITaskRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.model.IProjectService;
import ru.t1.kruglikov.tm.api.service.model.ITaskService;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.enumerated.TaskSort;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kruglikov.tm.exception.field.*;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Task;


import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getColumnName());
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final TaskSort sort
    ) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getColumnName());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAll(userId)
                .stream()
                .filter(x -> x.getProject().getId().equals(projectId)).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setStatus(Status.NOT_STARTED);

        return add(userId, task);
    }

    @NotNull
    @Transactional
    private Task update(@Nullable final Task task) {
        if (task == null) throw new ProjectNotFoundException();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();

        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(project);

        update(task);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null || !projectId.equals(task.getProject().getId())) throw new TaskNotFoundException();
        task.setProject(null);

        update(task);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAll(repository.findAll()
                .stream()
                .filter(x -> x.getUser().getId().equals(userId)).collect(Collectors.toList()));
    }

}