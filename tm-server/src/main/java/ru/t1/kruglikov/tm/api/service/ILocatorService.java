package ru.t1.kruglikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kruglikov.tm.api.service.dto.IProjectDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ISessionDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ITaskDtoService;
import ru.t1.kruglikov.tm.api.service.dto.IUserDtoService;

public interface ILocatorService {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDtoService getSessionService();

}