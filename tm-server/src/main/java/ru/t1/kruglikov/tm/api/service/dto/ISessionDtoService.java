package ru.t1.kruglikov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;

import java.util.List;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
    @Nullable
    List<SessionDTO> findAll(@Nullable SessionSort sort);

}


