package ru.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kruglikov.tm.api.endpoint.ITaskRestEndpoint;
import ru.kruglikov.tm.model.Task;
import ru.kruglikov.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @GetMapping("/findAll")
    public  Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") final String id) {
        return taskService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(@RequestBody final Task task) {
        taskService.add(task);
        return task;
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final Task task) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") final String id) {
        taskService.removeById(id);
    }

}