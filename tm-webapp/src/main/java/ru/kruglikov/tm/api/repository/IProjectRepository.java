package ru.kruglikov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kruglikov.tm.model.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
}
