package ru.t1.kruglikov.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.service.EntityService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EntityListener implements MessageListener {

    @Autowired
    @NotNull
    private final EntityService loggerService;

    public EntityListener(final EntityService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        @NotNull final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String text = textMessage.getText();
        loggerService.log(text);
    }

}
